from tkinter import *
import math

# ---------------------------- CONSTANTS ------------------------------- #
PINK = "#e2979c"
RED = "#e7305b"
GREEN = "#9bdeac"
YELLOW = "#f7f5dd"
FONT_NAME = "Courier"
WORK_MIN = 25
SHORT_BREAK_MIN = 5  # 5
LONG_BREAK_MIN = 15
WIDTH = 3
reps = 0
timer_f = None
icons = []


# ---------------------------- TIMER RESET ------------------------------- #
def reset_timer():
    window.after_cancel(timer_f)
    canvas.itemconfig(timer, text="00:00")
    info_l.config(text="Timer")
    check_l.config(text='')
    icons.clear()
    global reps
    reps = 0
    reset_b.config(state='disabled')
    start_b.config(state='normal')


# ---------------------------- TIMER MECHANISM ------------------------------- #
def start_timer():
    start_b.config(state='disabled')
    reset_b.config(state='normal')
    global reps
    reps += 1
    w_sec = WORK_MIN * 60
    sb_sec = SHORT_BREAK_MIN * 60
    lb_sec = LONG_BREAK_MIN * 60
    if reps % 8 == 0:
        count_down(lb_sec)
        info_l.config(text="Break time!", fg=RED)
        icons.clear()
    elif reps % 2 == 0:
        count_down(sb_sec)
        info_l.config(text="Short break!", fg=PINK)
    else:
        count_down(w_sec)
        info_l.config(text="Work time!", fg=GREEN)


# ---------------------------- COUNTDOWN MECHANISM ------------------------------- #

def count_down(count):
    count_min = math.floor(count / 60)
    count_sec = count % 60
    # if count_sec < 10:
    #     count_sec = f'0{count_sec}'

    canvas.itemconfig(timer, text=f'{count_min:02}:{count_sec:02}')
    if count >= 0:
        global timer_f
        timer_f = window.after(1000, count_down, count - 1)
    else:
        start_timer()
        if reps % 2 == 0:
            print(reps)
            icons.append("✔")
            check_l.config(text=icons)


# ---------------------------- UI SETUP ------------------------------- #

window = Tk()
window.title("Pomodoro")
window.config(padx=120, pady=50, bg=YELLOW)

# PHOTO:
canvas = Canvas(width=200, height=224, bg=YELLOW, highlightthickness=0)

tomato_img = PhotoImage(file="tomato.png")  # ImageTk.PhotoImage(Image.open("tomato.png"))
canvas.create_image(100, 112, image=tomato_img)
timer = canvas.create_text(100, 120, text="00:00", fill="white", font=(FONT_NAME, 35, 'bold'))
canvas.grid(column=1, row=1)
#


# UI:
info_l = Label(text="Timer", fg=GREEN, bg=YELLOW, font=(FONT_NAME, 35, 'bold'))
info_l.grid(column=1, row=0)

start_b = Button(text="Start", command=start_timer, highlightbackground=YELLOW, width=WIDTH, highlightthickness=0,
                 bg=YELLOW)
start_b.grid(column=0, row=2)

reset_b = Button(text="Reset", command=reset_timer, width=WIDTH, highlightthickness=0, bg=YELLOW,
                 highlightbackground=YELLOW)
reset_b.grid(column=2, row=2)
reset_b.config(state='disabled')

check_l = Label(text="", fg=GREEN, font=('', 10, ''), highlightthickness=0, bg=YELLOW)
check_l.grid(column=1, row=3)
#

window.mainloop()
